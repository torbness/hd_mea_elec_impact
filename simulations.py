
import os
import sys
from os.path import join
import numpy as np
import pylab as plt
from matplotlib import colors
import dolfin as df


class Parameters:
    """ Parameter class to save parameters, and read mesh parameters from file.
    """
    def __init__(self, mesh_folder):
        self.mesh_folder = mesh_folder
        self.elec_z = 0

        # Conductivity in S/m
        self.sigma_S = 1.5
        self.sigma_E = 1e7 # Electrode conductivity
        self.sigma_II = 1  # Input Impedance

        self.read_in_parameter_file()

    def read_in_parameter_file(self):
        param_file = open(join(self.mesh_folder, 'parameters.geo'), 'r')
        param_lines = param_file.readlines()
        for param in param_lines:
            split = param.split()
            setattr(self, split[0], float(split[2][:-1]))
        param_file.close()


class Conductivity(df.Expression):
    """ This is the class for conductivity. It is used to make the conductivity tensor
    for FEM simulations. See FEniCS information about 'user-defined expressions by subclassing'
    for more information on how this works"""

    def __init__(self, params):
        self.params = params

    def return_conductivity_tensor(self, Vs):
        """ This will return the conductivity scalar for
        isotropic tissue.
        """
        self.sigma = df.Function(Vs)
        self.sigma.interpolate(self)
        return self.sigma

    def return_conductivity(self, x):
        if x[2] <= -self.params.EL_thickness:
            return self.params.sigma_II
        elif -self.params.EL_thickness < x[2] <= self.params.elec_z:
            return self.params.sigma_E
        else:
            return self.params.sigma_S

    def eval(self, value, x):
        """ This function is written like this so I can use return_conductivity() outside the class for
        debugging reasons """
        value[0] = self.return_conductivity(x)


class FEM_simulation:
    """ Main class for Finite Element Method simulation. Uses the software package FEniCS.
    """
    def __init__(self, out_folder, mesh_folder, charge_pos, params, electrode_grounded):
        self.out_folder = out_folder
        self.mesh_folder = mesh_folder
        self.charge_pos = charge_pos
        self.electrode_grounded = electrode_grounded
        self.params = params
        self.sim_name = 'sigma_S:%.2e_sigma_II:%.2e_sigma_E:%.2e_E_GND:%s' % (params.sigma_S,
                                                                        params.sigma_II,
                                                                        params.sigma_E,
                                                                        str(electrode_grounded))
        if not os.path.isdir(out_folder):
            os.mkdir(out_folder)

    def _load_mesh(self):
        """
        Loading pre-made Finite Element Meshes from files. Meshes are made with Gmsh
        """
        df.parameters["krylov_solver"]["relative_tolerance"] = 1e-10
        df.parameters["allow_extrapolation"] = False
        self.mesh = df.Mesh(join(mesh_folder, "set-up.xml"))
        self.subdomains = df.MeshFunction("size_t", self.mesh, join(self.mesh_folder, "set-up_physical_region.xml"))
        self.boundaries = df.MeshFunction("size_t", self.mesh, join(self.mesh_folder, "set-up_facet_region.xml"))
        Vs = df.FunctionSpace(self.mesh, "DG", 0)
        sigma_class = Conductivity(self.params)
        self.sigma = sigma_class.return_conductivity_tensor(Vs)


    def do_fem_simulation(self):
        """ The Finite Element simulations are done and
        saved to Python numpy (*.npy) files """

        solver_params = {'linear_solver': 'cg',
                         'preconditioner': 'ilu',
                         'LagrangeP': 2,
                         }
        if not hasattr(self, 'mesh'):
            self._load_mesh()
        V = df.FunctionSpace(self.mesh, "CG", solver_params["LagrangeP"])

        v = df.TestFunction(V)
        u = df.TrialFunction(V)
        a = df.inner(self.sigma * df.grad(u), df.grad(v))*df.dx

        # This corresponds to Neumann boundary conditions zero, i.e. all outer boundaries are insulating.
        L = df.Constant(0)*v*df.dx

        # Adding Dirichlet boundary condition, corresponding to grounding (zero) at outer surfaces
        # The 1 beneath means at the boundary marked as Physical Surface 1 in gmsh file
        bcs = [df.DirichletBC(V, df.Constant(0), self.boundaries, 1)]
        if self.electrode_grounded:
            bcs.append(df.DirichletBC(V, df.Constant(0), self.boundaries, 2))

        phi = df.Function(V)
        A = df.assemble(a, cell_domains=self.subdomains, exterior_facet_domains=self.boundaries)
        b = df.assemble(L, cell_domains=self.subdomains, exterior_facet_domains=self.boundaries)

        [bc.apply(A, b) for bc in bcs]

        # Adding point source of magnitude 1.0
        x_pos, y_pos, z_pos = charge_pos
        point = df.Point(x_pos, y_pos, z_pos)
        delta = df.PointSource(V, point, 1.0)
        delta.apply(b)
        df.solve(A, phi.vector(), b, solver_params["linear_solver"], solver_params["preconditioner"])

        self._extract_field_values(phi)


    def _extract_field_values(self, field):
        """
        Extracts values for a cross section of the field found by FEM simulation, and saves results to file.
        :param field: Solution of FEM simulation
        :return:
        """
        x = np.linspace(-0.1, 0.1, 501)
        z = np.linspace(-0.1, 0.1, 500)

        bb = df.BoundingBoxTree()
        bb.build(self.mesh)

        #  Extract field values in cross section
        field_crossection = np.zeros((len(z), len(x)))

        for col_idx in xrange(len(x)):
            for row_idx in xrange(len(z)):
                point = df.Point(x[col_idx], 0, z[row_idx])
                if len(bb.compute_collisions(point)) > 0:
                    value = field([x[col_idx], 0, z[row_idx]])
                else:
                    value = 0
                field_crossection[row_idx, col_idx] = value

        #  Extrace sigma values in plane
        sigma_crossection = np.zeros((len(z), len(x)))
        for col_idx in xrange(len(x)):
            for row_idx in xrange(len(z)):
                point = df.Point(x[col_idx], 0, z[row_idx])
                if len(bb.compute_collisions(point)) > 0:
                    value = self.sigma([x[col_idx], 0, z[row_idx]])
                else:
                    value = 0
                sigma_crossection[row_idx, col_idx] = value

        np.save(join(self.out_folder, 'x_%s.npy' % self.sim_name), x)
        np.save(join(self.out_folder, 'z_%s.npy' % self.sim_name), z)

        np.save(join(self.out_folder, 'field_crossection_%s.npy' % self.sim_name), field_crossection)
        np.save(join(self.out_folder, 'sigma_crossection_%s.npy' % self.sim_name), sigma_crossection)


    def plot_results(self):
        """ Plot the set-up and potential at xz-plane,
        as well as the potential in the center along the z-axis (depth)
        """

        x = np.load(join(self.out_folder, 'x_%s.npy' % self.sim_name))
        z = np.load(join(self.out_folder, 'z_%s.npy' % self.sim_name))

        field_crossection = np.load(join(self.out_folder, 'field_crossection_%s.npy' % self.sim_name))
        sigma_crossection = np.load(join(self.out_folder, 'sigma_crossection_%s.npy' % self.sim_name))

        x_zero_idx = np.argmin(np.abs(x - 0))
        center_z_values = field_crossection[:, x_zero_idx]

        fig = plt.figure(figsize=[15, 5])
        fig.suptitle('Charge position:%s;    Electrode grounded: %s'
                            % (str(self.charge_pos), str(self.electrode_grounded)))
        fig.subplots_adjust(wspace=0.5, bottom=0.25, top=0.85)
        ax1 = fig.add_subplot(131, aspect=1, ylabel='z [mm]', xlabel='x [mm]', title='Set up')
        ax2 = fig.add_subplot(132, aspect=1, sharey=ax1, ylabel='z [mm]', xlabel='x [mm]',
                              title='Field cross section')
        ax3 = fig.add_subplot(133, ylim=(z[0], z[-1]), sharey=ax1,
                              ylabel='z [mm]', xlabel='Field strength a.u.')

        lines, line_names = self._plot_geometry_to_ax(sigma_crossection, x, z, ax1)
        cax1 = fig.add_axes([0.62, 0.25, 0.01, 0.6])

        img1 = ax2.imshow(field_crossection, interpolation='nearest', origin='lower',
                        extent=(x[0], x[-1], z[0], z[-1]))
        ax1.plot([x[x_zero_idx], x[x_zero_idx]], [np.min(z), np.max(z)], lw=2, c='g')

        plt.colorbar(img1, cax=cax1)
        l, = ax3.plot(center_z_values, z, lw=2, c='g')
        lines.append(l)
        line_names.append('Center field value')

        fig.legend(lines, line_names, frameon=False, loc=3, ncol=4, numpoints=1)
        plt.savefig('results_%s.png' % self.sim_name)

    def plot_mesh_3D(self):
        if not hasattr(self, 'mesh'):
            self._load_mesh()
        df.plot(self.mesh, title="Mesh")
        df.plot(self.subdomains, title="Subdomains")
        df.plot(self.boundaries, title="Boundaries")
        df.plot(self.sigma, title="Conductivity")

        df.interactive()

    def _plot_geometry_to_ax(self, sigma_cross, x, z, ax):
        """
        :param sigma_cross: Cross section of conductivity
        :param x: x-positions of points with conductivity values
        :param z: z-positions of points with conductivity values
        :param ax: matplotlib axis object to plot set-up to.
        :return: lines and line names for figure legend
        """
        def map_conductivity(value, plot_dict):
            for params in plot_dict.values():
                if np.abs(value - params[0]) <= 1e-10:
                    return params[1]
            raise RuntimeError("Value not here?")

        plot_dict = {'Input Impedance': [params.sigma_II, 1],
                     'Saline Bath': [params.sigma_S, 2],
                     'Electrode': [params.sigma_E, 3],
                     'Air': [0, 4]
                     }
        conductivity = np.zeros(sigma_cross.shape)
        for col_idx in xrange(len(x)):
            for row_idx in xrange(len(z)):
                conductivity[row_idx, col_idx] = map_conductivity(sigma_cross[row_idx, col_idx],
                                                                       plot_dict)

        bounds = [1, 2, 3, 4, 5]
        geom_colors = ['0.2', 'b', '0.8', 'c']

        line_names = ['Input Impedance: %g S/m' % params.sigma_II,
                 'Saline Bath: %g S/m' % params.sigma_S,
                 'Electrode: %g S/m' % params.sigma_E,
                 'Glass plate: 0 S/m',
                 'Current source']

        # make a color map of fixed colors
        cmap = colors.ListedColormap(geom_colors)
        norm = colors.BoundaryNorm(bounds, cmap.N)

        img = ax.imshow(conductivity, interpolation='nearest', origin='lower',
                        extent=(x[0], x[-1], z[0], z[-1]),
                         cmap=cmap, norm=norm)

        # Dummy points, just to get the labels
        l1, = ax.plot([100, 0], geom_colors[0], lw=10)
        l2, = ax.plot([100, 0], geom_colors[1], lw=10)
        l3, = ax.plot([100, 0], geom_colors[2], lw=10)
        l4, = ax.plot([100, 0], geom_colors[3], lw=10)
        # Plotting point charges
        l5, = ax.plot(self.charge_pos[0], self.charge_pos[2], '*y', ms=16)
        lines = [l1, l2, l3, l4, l5]

        ax.axis((x[0], x[-1], z[0], z[-1]))

        return lines, line_names

if __name__ == '__main__':

    mesh_folder = 'mesh'
    out_folder = 'results'
    params = Parameters(mesh_folder)
    charge_pos = [0, 0, 0.03]
    electrode_grounded = True
    fem = FEM_simulation(out_folder, mesh_folder, charge_pos, params, electrode_grounded)

    # fem.plot_mesh_3D()
    fem.do_fem_simulation()
    fem.plot_results()

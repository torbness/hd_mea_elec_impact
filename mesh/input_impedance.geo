
// Input Impedance (II)


p5i = newp; Point(p5i) = {0, 0, -2*EL_thickness, mesh_size_EL};
p6i = newp; Point(p6i) = {-EL_radius, 0, -2*EL_thickness, mesh_size_EL};
p7i = newp; Point(p7i) = {0, EL_radius, -2*EL_thickness, mesh_size_EL};
p8i = newp; Point(p8i) = {EL_radius, 0, -2*EL_thickness, mesh_size_EL};
p9i = newp; Point(p9i) = {0, -EL_radius, -2*EL_thickness, mesh_size_EL};

// Lower Circle
l4i = newl; Circle(l4i) = {p6i, p5i, p7i};
l5i = newl; Circle(l5i) = {p7i, p5i, p8i};
l6i = newl; Circle(l6i) = {p8i, p5i, p9i};
l7i = newl; Circle(l7i) = {p9i, p5i, p6i};

// Vertical Lines
l8 = newl; Line(l8) = {p6e, p6i};
l9 = newl; Line(l9) = {p7e, p7i};
l5e0 = newl; Line(l5e0) = {p8e, p8i};
l5e1 = newl; Line(l5e1) = {p9e, p9i};

ll6e_II = newll; Line Loop(ll6e_II) = {l4i,l5i,l6i,l7i};
ll7e_II = newll; Line Loop(ll7e_II) = {l4e, l9, -l4i, -l8};
ll4_II = newll; Line Loop(ll4_II) = {l5e, l5e0, -l5i, -l9};
ll5_II = newll; Line Loop(ll5_II) = {l6e, l5e1, -l6i, -l5e0};
ll6_II = newll; Line Loop(ll6_II) = {l7e, l8, -l7i, -l5e1};

lo_surf_II = news; Ruled Surface(lo_surf_II) = {ll6e_II};
vert_s1_II = news; Ruled Surface(vert_s1_II) = {ll7e_II};
vert_s2_II = news; Ruled Surface(vert_s2_II) = {ll4_II};
vert_s3_II = news; Ruled Surface(vert_s3_II) = {ll5_II};
vert_s4_II = news; Ruled Surface(vert_s4_II) = {ll6_II};


// Simple example set-up of Michigan type electrode in 'approximately infinite' half-space.
// The mesh is divided into Outer Tissue (OT), Inner Tissue (IT), ELectrode (EL)
// The reason why the tissue is divided in two parts is that is gives better control of the mesh size.  

// OT = Outer Tissue
// IT = Inner Tissue
// EL = ELectrode

// All sizes given in [mm]

Include 'parameters.geo';

// The mesh sizes determines how long the computations take.
// Increase it if you want to play around with less delay.
mesh_size_OT = 1.;
mesh_size_EL = 0.001;
mesh_size_IT = 0.005;

EL_surfaces[] = {};
EL_line_loops[] = {};
EL_volumes[] = {};

Include 'electrode.geo';
Include 'input_impedance.geo';

Include 'inner_tissue.geo';
Include 'outer_tissue.geo';

EL_surface_loop = newsl; Surface Loop(EL_surface_loop) = {up_surf_EL, lo_surf_EL, 
		vert_s1_EL, vert_s2_EL, vert_s3_EL, vert_s4_EL};
EL_volume = newv; Volume(EL_volume) = {EL_surface_loop};

II_surface_loop = newsl; Surface Loop(II_surface_loop) = {lo_surf_EL, lo_surf_II,
		vert_s1_II, vert_s2_II, vert_s3_II, vert_s4_II};
II_volume = newv; Volume(II_volume) = {II_surface_loop};

OT_surface_loop = newsl; Surface Loop(OT_surface_loop) =
		{vert_OT_surf1, vert_OT_surf2, vert_OT_surf3, vert_OT_surf4, lo_OT_surf, up_OT_surf,
                 vert_IT_surf1, vert_IT_surf2, vert_IT_surf3, vert_IT_surf4, up_IT_surf};

OT_volume = newv; Volume(OT_volume) = {OT_surface_loop};

IT_surface_loop = newsl; Surface Loop(IT_surface_loop) =
	       {vert_IT_surf1, vert_IT_surf2, vert_IT_surf3, vert_IT_surf4,
	       up_IT_surf, lo_IT_surf, up_surf_EL};

IT_volume = newv; Volume(IT_volume) = {IT_surface_loop};

Physical Volume(1) = {IT_volume, OT_volume};
Physical Volume(2) = {EL_volume};
Physical Volume(3) = {II_volume};

Physical Surface(1) = {vert_OT_surf1, vert_OT_surf2, vert_OT_surf3, vert_OT_surf4, up_OT_surf};
Physical Surface(2) = {lo_surf_II};

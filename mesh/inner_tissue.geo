
// Inner Tissue (IT)

p0 = newp; Point(p0) = {0, 0, 0, mesh_size_IT};
p1 = newp; Point(p1) = {IT_radius, 0, 0, mesh_size_IT};
p2 = newp; Point(p2) = {0, IT_radius, 0, mesh_size_IT};
p3 = newp; Point(p3) = {-IT_radius, 0, 0, mesh_size_IT};
p4 = newp; Point(p4) = {0, -IT_radius, 0, mesh_size_IT};
p5 = newp; Point(p5) = {IT_radius, 0, IT_thickness, mesh_size_IT};
p6 = newp; Point(p6) = {0, 0, IT_thickness, mesh_size_IT};
p7 = newp; Point(p7) = {0, IT_radius, IT_thickness, mesh_size_IT};
p8 = newp; Point(p8) = {-IT_radius, 0, IT_thickness, mesh_size_IT};
p9 = newp; Point(p9) = {0, -IT_radius, IT_thickness, mesh_size_IT};

l1 = newl; Circle(l1) = {p1, p0, p2};
l2 = newl; Circle(l2) = {p2, p0, p3};
l3 = newl; Circle(l3) = {p3, p0, p4};
l4 = newl; Circle(l4) = {p4, p0, p1};
l5 = newl; Circle(l5) = {p5, p6, p7};
l6 = newl; Circle(l6) = {p7, p6, p8};
l7 = newl; Circle(l7) = {p8, p6, p9};
l8 = newl; Circle(l8) = {p9, p6, p5};
l9 = newl; Line(l9) = {p1, p5};
l10 = newl; Line(l10) = {p2, p7};
l11 = newl; Line(l11) = {p3, p8};
l12 = newl; Line(l12) = {p4, p9}; 

up_IT_ll = newll; Line Loop(up_IT_ll) = {l5,l6,l7,l8};
up_IT_surf = news; Ruled Surface(up_IT_surf) = {up_IT_ll};

lo_IT_ll = newll; Line Loop(lo_IT_ll) = {l1,l2,l3,l4};
lo_IT_surf = news; Ruled Surface(lo_IT_surf) = {lo_IT_ll, ll1_EL};

// VERTICAL SURFACES
vert_IT_ll1 = newll; Line Loop(vert_IT_ll1) = {l1,l10,-l5,-l9};
vert_IT_surf1 = news; Ruled Surface(vert_IT_surf1) = {vert_IT_ll1};

vert_IT_ll2 = newll; Line Loop(vert_IT_ll2) = {l2,l11,-l6,-l10};
vert_IT_surf2 = news; Ruled Surface(vert_IT_surf2) = {vert_IT_ll2};

vert_IT_ll3 = newll; Line Loop(vert_IT_ll3) = {l3,l12,-l7,-l11};
vert_IT_surf3 = news; Ruled Surface(vert_IT_surf3) = {vert_IT_ll3};

vert_IT_ll4 = newll; Line Loop(vert_IT_ll4) = {l4,l9,-l8,-l12};
vert_IT_surf4 = news; Ruled Surface(vert_IT_surf4) = {vert_IT_ll4};



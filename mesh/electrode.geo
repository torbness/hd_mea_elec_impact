
// ELectrode (EL)


p0 = newp; Point(p0) = {0, 0, 0, mesh_size_EL};
p1 = newp; Point(p1) = {-EL_radius, 0, 0, mesh_size_EL};
p2 = newp; Point(p2) = {0, EL_radius, 0, mesh_size_EL};
p3 = newp; Point(p3) = {EL_radius, 0, 0, mesh_size_EL};
p4 = newp; Point(p4) = {0, -EL_radius, 0, mesh_size_EL};

p5e = newp; Point(p5e) = {0, 0, -EL_thickness, mesh_size_EL};
p6e = newp; Point(p6e) = {-EL_radius, 0, -EL_thickness, mesh_size_EL};
p7e = newp; Point(p7e) = {0, EL_radius, -EL_thickness, mesh_size_EL};
p8e = newp; Point(p8e) = {EL_radius, 0, -EL_thickness, mesh_size_EL};
p9e = newp; Point(p9e) = {0, -EL_radius, -EL_thickness, mesh_size_EL};

// Upper Circle
l0 = newl; Circle(l0) = {p1, p0, p2};
l1 = newl; Circle(l1) = {p2, p0, p3};
l2 = newl; Circle(l2) = {p3, p0, p4};
l3 = newl; Circle(l3) = {p4, p0, p1};

// Lower Circle
l4e = newl; Circle(l4e) = {p6e, p5e, p7e};
l5e = newl; Circle(l5e) = {p7e, p5e, p8e};
l6e = newl; Circle(l6e) = {p8e, p5e, p9e};
l7e = newl; Circle(l7e) = {p9e, p5e, p6e};

// Vertical Lines
l8 = newl; Line(l8) = {p1, p6e};
l9 = newl; Line(l9) = {p2, p7e};
l10 = newl; Line(l10) = {p3, p8e};
l11 = newl; Line(l11) = {p4, p9e};

ll1_EL = newll; Line Loop(ll1_EL) = {l0,l1,l2,l3};
ll2_EL = newll; Line Loop(ll2_EL) = {l4e,l5e,l6e,l7e};
ll3_EL = newll; Line Loop(ll3_EL) = {l0, l9, -l4e, -l8};
ll4_EL = newll; Line Loop(ll4_EL) = {l1, l10, -l5e, -l9};
ll5_EL = newll; Line Loop(ll5_EL) = {l2, l11, -l6e, -l10};
ll6_EL = newll; Line Loop(ll6_EL) = {l3, l8, -l7e, -l11};

up_surf_EL = news; Ruled Surface(up_surf_EL) = {ll1_EL};
lo_surf_EL = news; Ruled Surface(lo_surf_EL) = {ll2_EL};
vert_s1_EL = news; Ruled Surface(vert_s1_EL) = {ll3_EL};
vert_s2_EL = news; Ruled Surface(vert_s2_EL) = {ll4_EL};
vert_s3_EL = news; Ruled Surface(vert_s3_EL) = {ll5_EL};
vert_s4_EL = news; Ruled Surface(vert_s4_EL) = {ll6_EL};
